#include <stdio.h>
#include <stdbool.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>

int global_error;
struct str_arr folds_to_sync;
struct options opt;

struct options {
	bool include_hidden;
	bool compatibility_check;
	bool backup;
	bool last_modified_replace;
	bool verbose;
	bool from_first_to_second;
	bool from_second_to_first;
	char * folder_one;
	char * folder_two;
};


struct str_arr {
	char ** array;
	int size;
	int realloc_gap;
};


void str_arr_add(struct str_arr * arr, char * str) {
	(arr->size)++;
	// Add new spaces in arr->array for 'realloc_gap' new pointer in the array if we need more room
	if(arr->size == 1)
		arr->array = malloc(arr->realloc_gap * sizeof(char *));
		
	if(arr->size % arr->realloc_gap == 0) {
		arr->array = realloc(arr->array,  floor((arr->size / 8) + 1) * arr->realloc_gap * sizeof(char *));
	}

	// Asign a place for the new string 	
	*(arr->array + arr->size - 1) = malloc(strlen(str) * sizeof(char) + 1);

	// Copy the new string
	strcpy(*(arr->array + arr->size - 1), str);
}
void str_arr_free(struct str_arr * arr) {
	if(arr->size != 0) { 
		for(int i = 0; i < arr->size; i++) {
			free(*(arr->array + i));
		}
		free(arr->array);
	}

}

// Return the index of the str in the str_array, or -1 if not found
int str_arr_find(struct str_arr * arr, char * str) {
	for(int i = 0; i < arr->size; i++) {
		if(strcmp(*(arr->array + i), str) == 0)
			return i;
	}
	return -1;
}

void str_arr_show(struct str_arr * arr) {
	printf("Array : \n");
	for(int i = 0; i < arr->size; i++) {
		printf("array[%d] : %s\n", i, *(arr->array + i));
	}
}

void set_dir(struct options * opt, bool second, char* str) {
	if(second) {
		opt->folder_two = malloc(512 * sizeof(char));
		strcpy(opt->folder_two, str);
	} else {
		opt->folder_one = malloc(512 * sizeof(char));
		strcpy(opt->folder_one, str);
	}
	
}

void show_help();
//void sync_fold(char * fold_one_path, char * fold_two_path);
void sync_fold();
bool folder_sync(struct options * opt);
void format_to_dir(char * str);
void format_to_file(char * str);


int main(int argc, char **argv) {
	int dir_set_counter = 0;
	opt.backup = false;
	opt.compatibility_check = false;
	opt.include_hidden = false;
	opt.last_modified_replace = true;
	opt.verbose = false;
	opt.from_first_to_second = true;
	opt.from_second_to_first = true;
	
	for(int i = 1; i < argc; i++) {
		if(*(argv[i]) == '-') {
			for(int j = 1; j < strlen(argv[i]); j++) {
				switch(argv[i][j]) {
				case 'b':
					opt.backup = true;
					break;
				case 'c':
					opt.compatibility_check = true;
					break;
				case 'i':
					opt.include_hidden = true;
					break;
				case 's':
					opt.last_modified_replace = false;
					break;
				case 'v':
					opt.verbose = true;
					break;
				case '<':
				// Just disable copy from first to second, from_second_to_first should be enabled by default
					opt.from_first_to_second = false;
					break;
				case '>':
					opt.from_second_to_first = false;
					break;
				case 'h':
					show_help();
					return 0;
					
					
				default:
					printf("Unrecognized parameter : -%c\n", argv[i][j]);
					show_help();
					return 0;
				}
			}
		} else {
			if(dir_set_counter > 1) {
				printf("Error : Unrecognized argument : %s\nType nsscp -h for help\n", argv[i]);
				return 0;
			}
			set_dir(&opt, dir_set_counter, argv[i]);
			dir_set_counter++;
		}
	}
	
	if(!opt.from_first_to_second && !opt.from_second_to_first) {
		printf("Error : Arguments -< and -> are incompatible ; To synchronise from both directories to both directories, just don't specify any of -< and ->\n");
		return 0;
	} 
	
	
	
	folds_to_sync.size = 0;
	folds_to_sync.realloc_gap = 8;
	str_arr_add(&folds_to_sync, "");
	
	
	if(dir_set_counter < 2) {
		printf("Error : Need two directories to synchronise them...\nType nsscp -h for help\n");
		return 0;
	}

	// Make sure it ends with '/'
	format_to_dir(&(opt.folder_one[0]));
	format_to_dir(&(opt.folder_two[0]));
	
	char * save_one = malloc(strlen(&(opt.folder_one[0])) + 1);
	char * save_two = malloc(strlen(&(opt.folder_two[0])) + 1);
	strcpy(save_one, &(opt.folder_one[0]));
	strcpy(save_two, &(opt.folder_two[0]));
	
	printf("Synchronising %s and %s\n", opt.folder_one, opt.folder_two);
	
	
	sync_fold();
	if(global_error != 0)
		printf("error : %d\n", global_error);
	else 
		printf("Successfuly synchronised %s and %s\n", save_one, save_two); 
	str_arr_free(&folds_to_sync);
	free(save_one);
	free(save_two);

	return 0;
}

void show_help() {
	printf("NotSoStupidCopier - Alpha 0.7\n\
Folder synchroniser - For USB Stick, or external disks...\n\
Syncrhonise two directories, so that they're the same at the end\n\n\
Usage: nsscp [option] [FIRST DIRECORY PATH] [SECOND DIRECORY PATH]\n\
-b      Backup : Backup files (in the same direcory, with ~ at the end of the name\n\
-i      Include hidden files\n\
-h      Help : Show (this) help message\n\
-v      Verbose : Show each action\n\
-s      lesser Synchronisation : Do not syncrhonise modified files, only files not presents in both directories\n\
'->'    Only synchronise from first to second directory (you MUST surround this option with '')\n\
'-<'    Only synchronise from second to first directory (you MUST surround this option with '')\n\
        (By default, synchronise from and to both directories)\n\n\
Example : nsscp /home/user/ /media/user/XXXX-XXXX/ -v -i '->' \n\
         will synchronyse from the /home/user/ to /media/user/XXXX-XXXX/ (example of an usb stick), \n\
         creating backups and showing every actions\n\
By Gétéhèn\n");
}

// file_or_fold -> 0 = looking for a file, 1 = looking for a dir
int find_file(char * name, char * searching_dir, int type) {
	struct dirent * entry;
	DIR * dir = opendir(searching_dir);
	if(searching_dir != NULL) {
		while ((entry = readdir (dir)) != NULL) {
			if(strcmp(entry->d_name, name) == 0) {
				if(entry->d_type == type) {
					closedir(dir);
					return 0;
				}
				else {
					closedir(dir);
					return 1;
				}
;
			}
		}
		global_error = 0;
		closedir(dir);
		return -1;
	} else {
		/* could not open directory */
		global_error = 1;
		closedir(dir);
		return -1;
	} 
}

// Return -1 if error
// Return 0 if no one is older than the other one (less than 2 seconds diff) 
// Return 1 if two is older than one
// Return 2 if one is older than two
int is_older(char * one, char * two) {
	struct stat st_one;
	struct stat st_two;
	if( stat(one, &st_one) == -1 || stat(two, &st_two) == -1) {
		global_error = 1;
		return -1;
	}
	time_t t_one = st_one.st_mtime;
	time_t t_two = st_two.st_mtime;
	if(abs(t_one - t_two) <= 2) {
		return 0;
	}
	if(t_one < t_two) {
		return 1;
	} else {
		return 2;
	}
}


bool copy(char * from, char * to) {
	char cmd[2048] = "";
	strcat(cmd, "cp -p");

	if(opt.backup == true)
		strcat(cmd, "b");

	if(*(from + strlen(from) - 1) == '/')
		strcat(cmd, "r");

	strcat(cmd, " '");
	strcat(cmd, from);
	strcat(cmd, "' '");
	strcat(cmd, to);
	strcat(cmd, "'");
	
	system(cmd);
	if(opt.verbose)
		printf("Copying from %s to %s\n", from, to);
	return 0;
}

bool rm(char * path) {
	char cmd[2048] = "";
	strcat(cmd, "gio trash '");
	strcat(cmd, path);
	strcat(cmd, "'");
	system(cmd);
	if(opt.verbose)
		printf("Putting %s in the trash\n", path);
	return 0;
}

void format_to_dir(char * str) {
	if(*(str + strlen(str) - 1) != '/')
		strcat(str, "/");
}

void format_to_file(char * str) {
	if(*(str + strlen(str) - 1) == '/')
		*(str + strlen(str) - 1) = '\0';
}

//void sync_fold(char * dir_one_path, char * dir_two_path) {
void sync_fold() {
	char * dir_one_path = &(opt.folder_one[0]);
	char * dir_two_path = &(opt.folder_two[0]);

	DIR * dir_one;
	DIR * dir_two;
	struct dirent * entry_one;
	struct dirent * entry_two;	



	// Set up some space for paths
	dir_one_path = realloc(dir_one_path, 256 * sizeof(char));
	dir_two_path = realloc(dir_two_path, 256 * sizeof(char));
	
	// dir_x_path_len is used to truncate the string to the end of base paths
	int dir_one_path_len = strlen(dir_one_path);
	int dir_two_path_len = strlen(dir_two_path);
	
	// As the paths size can be growing, we need to know the max we can handle 
	int dir_one_path_max = 256;
	int dir_two_path_max = 256;
	int buffer; 

	char path_buffer[2048] = "";
	
	// Counter is used to count the copied dirs, not to process when parsing the second folder 
	int counter = 0;

//	condition : (counter < folds_to_sync.size)
	do {
		// To avoid copying from first to second directory, and then, when parsing second directory, copying from second to first, we stock the files already handled in copied_arr
		struct str_arr copied_arr;
		copied_arr.realloc_gap= 8;
		copied_arr.size = 0;

		
		// Cut to the end of the base paths
		dir_one_path[dir_one_path_len] = '\0';
		dir_two_path[dir_two_path_len] = '\0';
		buffer = strlen(*(folds_to_sync.array + counter)) + dir_one_path_len;
		// If the new path is too long, add 256 bits to it
		while(buffer >= dir_one_path_max) {
			buffer = (floor(buffer / 256) + 1) * 256 * sizeof(char);
			dir_one_path = realloc(dir_one_path, buffer);
			dir_one_path_max = buffer;
		}
		
		buffer = strlen(*(folds_to_sync.array + counter)) + dir_two_path_len;
		while(buffer >= dir_two_path_max) {
			buffer = (floor(buffer / 256) + 1) * 256 * sizeof(char);
			dir_two_path = realloc(dir_two_path, buffer);
			dir_two_path_max = buffer;
		}
		
		// Make the new path : base_path + directory_currently_parsed
		strcat(dir_one_path, *(folds_to_sync.array + counter));
		strcat(dir_two_path, *(folds_to_sync.array + counter));

		// If we can't open on of the dirs
		if ((dir_one = opendir (dir_one_path)) == NULL) {
			global_error = 1;
			counter ++;
			str_arr_free(&copied_arr);
			printf("Can't open directory : %s\n", dir_one_path);
			continue;
		}
		
		if ((dir_two = opendir (dir_two_path)) == NULL) {
			global_error = 1;
			counter ++;
			str_arr_free(&copied_arr);
			printf("Can't open directory : %s\n", dir_two_path);
			continue;
		}
		if(opt.verbose)
			printf("Entering %s\n",dir_one_path);
		
		// Parse the first dir
		while ((entry_one = readdir (dir_one)) != NULL) {
			// Reset the buffer
			path_buffer[0] = '\0';
			
			// If the folder is ./ (itself) or ../ (its parent), don't handle it
			if(strcmp(entry_one->d_name, ".") == 0 || strcmp(entry_one->d_name, "..") == 0 || *(entry_one->d_name + strlen(entry_one->d_name) - 1) == '~') 
				continue;
				
			if(opt.include_hidden == false && *(entry_one->d_name) == '.')
				continue;
			
			// Add the dir to copied_arr array
			str_arr_add(&copied_arr, entry_one->d_name);
//			printf ("Entry (one) : %s", entry_one->d_name);
			
			// If it's a file
			if(entry_one->d_type == DT_REG) {
				
				strcat(path_buffer, dir_one_path);
				strcat(path_buffer, entry_one->d_name);

				// If it's also in the second dir and it's a file too (buffer == 0), copy the one older. Else (-1), copy it to the second dir. If it's not a file (1), skip it but tell the user
				buffer = find_file(entry_one->d_name, dir_two_path, DT_REG);
				if(buffer == 0) {
					// If not opt.last_modified_replace, skip date checking and continue to the next file
					if(!opt.last_modified_replace)
						continue;
					char * path_buffer_2 = malloc(2048 * sizeof(char));
					memset(path_buffer_2, '\0', 2048);
					strcat(path_buffer_2, dir_two_path);
					strcat(path_buffer_2, entry_one->d_name);

					// Check the date
					buffer = is_older(path_buffer, path_buffer_2);
					// 1 -> second is younger. 2 -> first is younger
					if(buffer == -1)
						printf("Warning: cannot handle: %s or %s (Cannot get last modification time)\n", path_buffer, path_buffer_2);
					if(buffer == 1) {
						if(opt.from_second_to_first) {
							if(opt.backup == false)
								rm(path_buffer);
							copy(path_buffer_2, dir_one_path);
						}
					} else {
						if(opt.from_first_to_second) {
							if(buffer == 2) {
								if(opt.backup == false)
									rm(path_buffer_2);
								copy(path_buffer, dir_two_path);
							}
						}
					}
					free(path_buffer_2);
				} else {
					if(buffer == -1 && opt.from_first_to_second) {
						copy(path_buffer, dir_two_path);
					} else {
						printf("WARNING : A folder already exist in %s with the name %s. However, we have to handle a file\nSkipping file : %s\n", dir_two_path, entry_one->d_name, path_buffer);
					}
				}
			}
			
			// If the file is a dir
			if(entry_one->d_type == DT_DIR) {
//				printf(" (dir)\n");
			
				// If it's not in the second dir and it's a folder too (buffer == -1) copy it. Else (0), add it to folds_to_sync. If it's not a folder (1), skip it but tell the user
				buffer = find_file(entry_one->d_name, dir_two_path, DT_DIR);
				if(buffer == -1 && opt.from_first_to_second) {
					strcat(path_buffer, dir_one_path);
					strcat(path_buffer, entry_one->d_name);
					format_to_dir(path_buffer);
					copy(path_buffer, dir_two_path);
				} else {
					if(buffer == 0) {
						strcat(path_buffer, * (folds_to_sync.array + counter));
						strcat(path_buffer, entry_one->d_name);
						// Make sure it ends with '/'
						format_to_dir(path_buffer);
						str_arr_add(&folds_to_sync, path_buffer);
					} else {
						if(opt.from_first_to_second)
							printf("WARNING : A file already exist in %s with the name %s. However, we have to handle a file\nSkipping folder : %s\n", dir_two_path, entry_one->d_name, path_buffer);
					}
				}
			}
//			printf("\n");
		}
		closedir (dir_one);
		if(opt.verbose)
			printf("Leaving %s\n",dir_one_path);
		
		if(opt.verbose)
			printf("Entering %s\n",dir_two_path);
		
		// Parse the second dir ONLY IF FROM_SECOND_TO_FIRST IS TRUE
		if(opt.from_second_to_first) {
			while ((entry_two = readdir (dir_two)) != NULL) {
				// Clear path_buffer
				path_buffer[0] = '\0';

				// If the folder is ./ (itself) or ../ (its parent), don't handle it
				if(strcmp(entry_two->d_name, ".") == 0 || strcmp(entry_two->d_name, "..") == 0 || *(entry_two->d_name + strlen(entry_two->d_name) - 1) == '~') 
					continue;
				
				if(opt.include_hidden == false && *(entry_two->d_name) == '.')
					continue;

				// If it's a file
				if(entry_two->d_type == DT_REG) {
					// If it's not in the copied_arr array, it means that there's no file / folder with that name in the first dir, so we copy it to the first dir
					if(str_arr_find(&copied_arr, entry_two->d_name) != -1) {
						continue;
					}
					strcat(path_buffer, dir_two_path);
					strcat(path_buffer, entry_two->d_name);
					copy(path_buffer, dir_one_path);
				}
				
				// If the file is a dir
				if(entry_two->d_type == DT_DIR) {

					// If the file is in copied_arr, it has already been processed
					if(str_arr_find(&copied_arr, entry_two->d_name) != -1) {
						continue;
					}

					// If it's not in copied_arr, it means that there's no file / folder with that name in the first dir, so we copy it to the first dir
					strcat(path_buffer, dir_two_path);
					strcat(path_buffer, entry_two->d_name);
					format_to_dir(path_buffer);
					copy(path_buffer, dir_one_path);
					
				}
			}
		}
		
		closedir (dir_two);
		if(opt.verbose)
			printf("Leaving %s\n",dir_two_path);
		
		counter ++;
		str_arr_free(&copied_arr);
	} while(counter < folds_to_sync.size);
	free(dir_one_path);
	free(dir_two_path);
	
}