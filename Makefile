.PHONY: clean All

All:
	@echo "----------Building project:[ nsscp_v3 - Debug ]----------"
	@"$(MAKE)" -f  "nsscp_v3.mk"
clean:
	@echo "----------Cleaning project:[ nsscp_v3 - Debug ]----------"
	@"$(MAKE)" -f  "nsscp_v3.mk" clean
