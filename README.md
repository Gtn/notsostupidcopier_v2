# NotSoStupidCopier

NotSoStupidCopier (NSSCP) is a copier used to synchronise two folders.

It check whether a file is present in both folders, and if it not it copy it to the second folder.
It also check if a file was modified, and if it has been modified, it override the old file with the new file (can be disabled if you just want to synchronise files without last modified time sync)

##How to install ?
Just put nsscp in /usr/bin/
To sync files, just open a terminal and type :
`nsscp [first dir] [second dir]`
type `nsscp -h` to show help message